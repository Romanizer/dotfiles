if lua_callstack then print("after/catppuccin.lua") end

require("catppuccin").setup({
    transparent_background = true,
})

vim.cmd [[colorscheme catppuccin]]

-- latte, frappe, macchiato, mocha
vim.g.catppuccin_flavour = "mocha"
