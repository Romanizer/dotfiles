if lua_callstack then print("after/telescope.lua") end

pcall(require('telescope').load_extension, 'fzf')
local builtin = require('telescope.builtin')

-- list files in current working dir, respecting gitignore
vim.keymap.set('n', '<leader>ff', builtin.find_files, {})

-- list files in git, respects gitignore
vim.keymap.set('n', '<leader>fg', builtin.git_files, {})

-- grep
vim.keymap.set('n', '<leader>ps', function()
    builtin.grep_string({ search = vim.fn.input("Grep > ") });
end)
