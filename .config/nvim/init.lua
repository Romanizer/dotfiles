--------------------------------
-- Roman's Awesome NeoVim config
--------------------------------

-- uncomment to see call stack on neovim start
-- lua_callstack = true

require("roman.init")
