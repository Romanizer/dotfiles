-- nvim options
require("roman.set")

-- keymap and custom commands
require("roman.map")

-- plugins, themes etc
require("roman.packer")
