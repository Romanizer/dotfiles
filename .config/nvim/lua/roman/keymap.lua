-- This script is only required if another script needs to access remap functions
-- This makes it easier by just writing:
--nnoremap("<C-d>", "<C-d>zz")

local M = {}

-- bind creates functions for various remapping usecases
-- the script exports an object that has these functions
local function bind(op, outer_opts)
    outer_opts = outer_opts or {noremap = true}
    return function(lhs, rhs, opts)
        opts = vim.tbl_extend("force",
            outer_opts,
            opts or {}
        )
        vim.keymap.set(op, lhs, rhs, opts)
    end
end

M.nmap = bind("n", {noremap = false})
M.nnoremap = bind("n")
M.vnoremap = bind("v")
M.xnoremap = bind("x")
M.inoremap = bind("i")

return M

