-- custom commands
------------------

-- cargo build commands for rust
vim.api.nvim_create_user_command('Rbr', '!cargo b -r', {})
vim.api.nvim_create_user_command('Rb', '!cargo b', {})

-- cargo run commands for rust
vim.api.nvim_create_user_command('Rrr', '!cargo r -r', {})
vim.api.nvim_create_user_command('Rr', '!cargo r', {})

-- cargo check command
vim.api.nvim_create_user_command('CC', '!cargo check', {})

-- cargo test command
vim.api.nvim_create_user_command('CT', '!cargo test', {})


-- remaps
---------
local nnoremap = require("roman.keymap").nnoremap

-- remap ctrlD/U to center cursor
nnoremap("<C-d>", "<C-d>zz")
nnoremap("<C-u>", "<C-u>zz")

-- remap search to center after jump
nnoremap("n", "nzzzv")
nnoremap("N", "Nzzzv")

nnoremap("<leader>pv", vim.cmd.Ex)
