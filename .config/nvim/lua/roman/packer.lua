----------
-- plugins
----------

-- ensure packer is installed
local ensure_packer = function()
  local fn = vim.fn
  local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
  if fn.empty(fn.glob(install_path)) > 0 then
    fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
    vim.cmd [[packadd packer.nvim]]
    return true
  end
  return false
end

local packer_bootstrap = ensure_packer()

-- load all plugins
require('packer').startup(function(use)

    -- Packer can manage itself
    use 'wbthomason/packer.nvim'

    -- LuaLine - A better status line --
    use { 'nvim-lualine/lualine.nvim',
        requires = { 'kyazdani42/nvim-web-devicons', opt = true }
    }
    require('lualine').setup()

    use {
        'saecki/crates.nvim',
        requires = { 'nvim-lua/plenary.nvim' },
    }
    require('crates').setup()

    -- LSP
    use {
        'VonHeikemen/lsp-zero.nvim',
        requires = {
            -- LSP Install etc
            {'williamboman/mason.nvim'},
            {'williamboman/mason-lspconfig.nvim'},
            -- status updates for LSP (?)
            {'j-hui/fidget.nvim', tag = 'legacy', opts = {} },
            -- LSP Support
            {'neovim/nvim-lspconfig'},

            -- Autocompletion
            {'hrsh7th/nvim-cmp'},
            --- LSP completion
            {'hrsh7th/cmp-nvim-lsp'},
            -- snippet engine for nvim-cmp
            {'L3MON4D3/LuaSnip'},
            {'saadparwaiz1/cmp_luasnip'},
            -- adds user friendly snippets
            {'rafamadriz/friendly-snippets'},

            -- other (?)
            {'hrsh7th/cmp-buffer'},
            {'hrsh7th/cmp-path'},
            {'hrsh7th/cmp-nvim-lua'},
        },
    }

    -- shows pending keybinds
    use { 'nvim-tree/nvim-web-devicons' }
    use { 'echasnovski/mini.nvim' }

    use { 'folke/which-key.nvim', opts = {} }

    -- Treesitter
    use {
        'nvim-treesitter/nvim-treesitter',
        run = function()
            pcall(require('nvim-treesitter.install').update { with_sync = true })
        end,
    }

    use {
        'nvim-treesitter/nvim-treesitter-textobjects',
        after = 'nvim-treesitter',
    }

    -- Telescope
    use {
      'nvim-telescope/telescope.nvim', tag = '0.1.3',
    -- or                            , branch = '0.1.x',
      requires = { {'nvim-lua/plenary.nvim'} }
    }

    -- tmux neovim navigator using ctrl+HJKL
    use { "alexghergh/nvim-tmux-navigation" }

    -- Theme
    use { 'catppuccin/nvim', as = 'catppuccin' }

    if packer_bootstrap then
        require('packer').sync()
    end
end)

