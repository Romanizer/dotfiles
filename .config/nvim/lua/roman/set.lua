local g = vim.g
local o = vim.o
local a = vim.api

g.mapleader = " "
g.maplocalleader = " "

o.termguicolors = true

-- decrease update time
o.timeoutlen = 500
o.updatetime = 200

-- num of scroll lines to keep above and below cursor
o.scrolloff = 8

-- background transparent
a.nvim_set_hl(0, "Normal", { bg = "none" })
a.nvim_set_hl(0, "NormalFloat", { bg = "none" })

-- editor tweaks
o.number = true
o.numberwidth = 3
o.relativenumber = true
o.signcolumn = 'yes'
o.cursorline = true

o.expandtab = true
o.smarttab = true
o.smartindent= true
o.autoindent = true
o.wrap = false
o.textwidth = 300
o.tabstop = 4
o.shiftwidth = 4
o.softtabstop = -1

o.hlsearch = false

o.clipboard = 'unnamedplus'

-- case insensitive searching UNLESS /C or capital in search
o.ignorecase = true
o.smartcase = true

-- backup undo etc
o.backup = false
o.writebackup = true
o.undofile = true
o.swapfile = false

o.history = 50
