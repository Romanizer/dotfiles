#!/bin/bash
### Romans Shell RC

# allow hot reloading .romanrc
alias reloadromanrc="source ~/.romanrc"

# set pico-sdk path if available
if [ -e "$HOME/coding/pico-sdk" ]
then
    export PICO_SDK_PATH=$HOME/coding/pico-sdk
fi

# source Xilinx settings for everything
# only if running in bash and Xilinx software is installed
if [ -n "$BASH_VERSION" ] && [ -e "$HOME/Xilinx" ]
then
    source ~/Xilinx/PetaLinux/settings.sh
    source ~/Xilinx/Vivado/2019.1/settings64.sh
    source ~/Xilinx/SDK/2019.1/settings64.sh
fi

if [ -e "$HOME/Xilinx" ]
then
    # fixes some weird locale issues since latest update
    export LC_ALL="en_US.UTF-8"
    alias vivado="~/Xilinx/Vivado/2019.1/bin/vivado"
fi

# append cargo install dir to path
export PATH="$HOME/.cargo/bin:$PATH"

if command -v exa &> /dev/null
then
    alias ll="exa -lag"
fi

if command -v eza &> /dev/null
then
    alias ll="eza -lag"
fi

if command -v lsd &> /dev/null
then
    # if lsd < 0.23.1 do not use git flag
    lsd_version="$(lsd --version)"
    if [[ "$lsd_version" == "lsd 0.23.1" ]]
    then
        alias ll="lsd -lA"
    else
        alias ll="lsd -lAg"
    fi
    alias tree="lsd --tree -l"
fi

# if ll was not assigned above, use ls fallback
if ! command -v ll &> /dev/null
then
    alias ll="ls -lA"
fi

alias l="ls -la"

alias n="nano"
alias v="nvim"
alias sv="sudo nvim"

# git stuff
alias gs="git status"
alias gl="git log"
alias gd="git diff"
alias gds="git diff --staged"
alias gup="git fetch && git pull"
alias gnuke="git restore . && git clean -dxf"

# source python venv in coding folder
alias spyvenv="source $HOME/coding/venv/bin/activate"

# reload latex files automatically
# only works with one .tex file per folder!
# lualatex seems to be the most compatible one, so far
alias ltex-reload="ls *.tex | entr -ps 'lualatex *.tex'"

# Exiftool removing image metadata (needs file as arg)
alias rm-image-meta="exiftool -all:all= -r"

# house keeping
# only use this orphans command if we have pacman
if command -v pacman &> /dev/null
then
    alias orphans="pacman -Qtdq"
    # alias orphans="expac '%n %m' -l'\n' -Q $(pacman -Qtdq) | sort -rhk 2 | less"
fi

# if not running interactively, return
# makes rsync etc work
[[ $- != *i* ]] && return

# only use neofetch and lolcat if available
if command -v fastfetch &> /dev/null
then
    if command -v lolcat &> /dev/null
    then
        fastfetch | lolcat
    else
        fastfetch
    fi
else
    if command -v neofetch &> /dev/null
    then
        if command -v lolcat &> /dev/null
        then
            neofetch | lolcat
        else
            neofetch
        fi
    fi
fi

randomize_wallpapers() {
    # Fuck Zsh, without this, we cant iterate over the output of find (list of dirs)
    setopt sh_word_split

    # list all dirs in wallpapers folder
    wallpapers=$(find ~/Pictures/wallpapers/ -maxdepth 1 -mindepth 1 -type d)

    rm -rf $HOME/Pictures/wallpapers/in-use-linked/*
    #gio trash $HOME/Pictures/wallpapers/in-use-linked/*

    gio trash $HOME/Pictures/wallpapers/in-use-copied/*

    for wallpaper in $wallpapers
    do
        # randomly decide the wallpapers
        if ((RANDOM % 2 == 0))
        then
            continue
        fi

        # Filter out old, in-use and in-use-animated folders
        if [[ $wallpaper == *in-use* || $wallpaper == *old* ]]
        then
            continue
        fi

        echo "linking $wallpaper"
        linked="$HOME/Pictures/wallpapers/in-use-linked/$(basename $wallpaper)"
        ln -s $wallpaper $linked

        #copied="$HOME/Pictures/wallpapers/in-use-copied/$(basename $wallpaper)"
        #cp -R $wallpaper $copied
        break
    done
}
