# dotfiles

Most of my configuration lives here.

## ISSUES 
### NeoVim: LSP `ltex-ls` not working

Check the logs using `:LspLog`.
```
[ERROR][2025-02-13 13:58:28] .../vim/lsp/rpc.lua:770	"rpc"	"ltex-ls"	"stderr"	"java.lang.UnsupportedClassVersionError: org/bsplines/ltexls/LtexLanguageServerLauncher has been compiled by a more recent version of the Java Runtime (class file version 55.0), this version of the Java Runtime only recognizes class file versions up to 52.0"
```

It's time to update to a higher java version!
Use `archlinuix-java` to set a new version:
```
# e.g. using v11
sudo archlinux-java set java-11-openjdk
```
