#!/bin/bash

function install_shell_rc {
    curr_date=$(date +%FT%T)
    dest="$HOME/.romanrc"
    path="$dest-$curr_date"

    # backup old romanrc file
    if [[ -f $dest ]]; then
        echo "backing up old shell_rc"
        mv "$dest" "$path"
    fi

    # install new from repo
    cp .romanrc ~/.romanrc

    echo "Now just create a hook in your shell's rc file:"
    echo "source ~/.romanrc"
    echo "Done.."
}

function install_shell_rc_fish {
    curr_date=$(date +%FT%T)
    dest="$HOME/.config/fish"
    path="$dest-bak-$curr_date"

    # backup old romanrc file
    if [[ -d $dest ]]; then
        echo "backing up old fish rc to $path"
        mv "$dest" "$path"
    fi

    # install new from repo
    cp -r ./.config/fish "$dest"

    # fishrc: .romanrc-fish
    dest="$HOME/.romanrc-fish"
    path="$dest-bak-$curr_date"
    # backup old romanrc file
    if [[ -f $dest ]]; then
        echo "backing up old .romanrc-fish"
        mv "$dest" "$path"
    fi

    # install new from repo
    cp .romanrc-fish ~/.romanrc-fish

    echo "Installed fish RC"
}

function install_nvim {
    curr_date=$(date +%FT%T)
    goal="$HOME/.config/nvim"
    path="$goal-bak-$curr_date"

    if [[ -d $goal ]]; then
        echo "backing up old config to $path"
        # backup old nvim folder with date
        mv "$goal" "$path"
    fi

    # copy nvim folder to config
    cp -r ./.config/nvim "$goal"
}

# install tmux config
function install_tmux {
    path="$HOME/.config/tmux"
    config="$path/tmux.conf"
    
    if [[ ! -d $path ]]; then
        echo "tmux config dir not present"
        mkdir $path
    fi

    # if config already exists, mv current to backup
    if [[ -f $config ]]; then
        mv "$path/tmux.conf"{,.bak}
    fi

    cp ./.config/tmux/tmux.conf $config
}

function install_neofetch {
    path="$HOME/.config/neofetch"
    config="$path/config.conf"
    
    if [[ ! -d $path ]]; then
        echo "neofetch config dir not present"
        mkdir $path
    fi

    # if config already exists, mv current to backup
    if [[ -f $config ]]; then
        mv "$path/config.conf"{,.bak}
    fi

    cp ./.config/neofetch/config.conf $config
}

# ask if nvim cfg should be installed
read -e -p "Install Roman's NeoVim config? [y/N] " choice
[[ "$choice" == [Yy]* ]] && install_nvim || echo "exiting..."

read -e -p "Install Roman's Neofetch config? [y/N] " choice
[[ "$choice" == [Yy]* ]] && install_neofetch || echo "exiting..."

read -e -p "Install Roman's tmux config? [y/N] " choice
[[ "$choice" == [Yy]* ]] && install_tmux || echo "exiting..."

read -e -p "Install Roman's shell RC? [y/N] " choice
[[ "$choice" == [Yy]* ]] && install_shell_rc || echo "exiting..."

read -e -p "Install Roman's fish RC? [y/N] " choice
[[ "$choice" == [Yy]* ]] && install_shell_rc_fish || echo "exiting..."
